import React, { useContext, useEffect } from 'react';
import { GlobalContext } from './Config/Context/global';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './Components/Header';
import Routes from './Routes';
import api from './Config/categories';

function App() {

  const { setMenu } = useContext(GlobalContext);

  useEffect(() => {
    const loadDataHeader = async () => {
      let res = await api.header();
      setMenu(res.items);
    }

    loadDataHeader();
  }, [setMenu]); 

  return (
    <Router>
      <Header />
      <Routes />
    </Router>
  );
}

export default App;

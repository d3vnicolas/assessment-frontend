import React from 'react';
import Main from '../../Components/Main';
import Footer from '../../Components/Footer';

const Home = () => {
    return (
        <>
            <Main />
            <Footer />
        </>
    );
}

export default Home;
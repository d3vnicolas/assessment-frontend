import React, { useEffect, useContext } from 'react';
import Footer from '../../Components/Footer';
import Shop from '../../Components/Shop';
import Filters from '../../Components/Filters';
import { useParams, Link } from 'react-router-dom';
import { PageContext } from '../../Config/Context/pages';
import api from '../../Config/categories';

const Calcas = () => {
    const { id } = useParams();
    const { setProducts } = useContext(PageContext);

    useEffect(() => {
        const loadProucts = async () => {
            let res = await api.products(id);
            setProducts(res);
        }

        loadProucts();
    }, [setProducts, id]);

    return (
        <>
            <main className="main">
                <div className="main__container">
                    <nav className="main__breadcrumb">
                        <ul>
                            <li><Link to="/">Página inicial</Link> &gt; </li>
                            <li>Calças</li>
                        </ul>
                    </nav>
                    <div className="main__content">
                        <Shop title="Calças" />
                        <Filters />
                    </div>
                </div>
            </main>
            <Footer />
        </>
    );
}

export default Calcas;
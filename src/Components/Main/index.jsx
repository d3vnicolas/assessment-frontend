import React, { useContext } from 'react';
import { GlobalContext } from '../../Config/Context/global';
import { Link } from 'react-router-dom';
import './main.scss';

const Main = () => {

    const { menu } = useContext(GlobalContext);

    return (
        <main className="main">
            <div className="main__container">
                <div className="main__content">
                    <section className="main__center">
                        <figure className="main__banner">
                            <div className="main__banner--img"></div>
                        </figure>
                        <article className="main__article">
                            <h1 className="main__article__text main__article--title">
                                Seja bem-vindo!
                            </h1>
                            <p className="main__article__text main__article--paragraph">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                                qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde
                                omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                                quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                                dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
                                qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia
                                non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam
                                quaerat voluptatem.
                            </p>
                        </article>
                    </section>
                    <aside className="main__left mobile-hidden">
                        <nav className="main__left__menu">
                            <ul>
                                <li><Link to="/">Página inicial</Link></li>
                                {menu.map((item, key) => (
                                    <li key={key}><Link to={`/${item.path}/${item.id}`}>{item.name}</Link></li>
                                ))}
                                <li><Link to="/contato">Contato</Link></li>
                            </ul>
                        </nav>
                    </aside>
                </div>
            </div>
        </main>
    );
}

export default Main;
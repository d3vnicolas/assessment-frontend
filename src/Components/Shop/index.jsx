import React, { useContext, useEffect } from 'react';
import './shop.scss';
import Card from '../Card';
import { BsGrid3X3GapFill } from 'react-icons/bs';
import { FaList } from 'react-icons/fa';
import { PageContext } from '../../Config/Context/pages';

const Shop = (props) => {

    const { products, view, setView } = useContext(PageContext);

    const list = {
        gridTemplateColumns: 'repeat(1, minmax(200px, 1fr))'
    };
    const grid = {
        gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))'
    };

    useEffect(() => {
        let grid = document.getElementById('grid');
        let list = document.getElementById('list');
        view ? grid.checked = true : list.checked = true;
    }, [view]);

    return (
        <section className="main__shop">
            <div className="main__title">
                <h1>{props.title}</h1>
            </div>
            <div className="main__view">
                <div className="main__view__options main__view__options--grid">
                    <input onChange={() => { setView(true) }} type="radio" name="view" value="1" id="grid" />
                    <label htmlFor="grid">
                        <BsGrid3X3GapFill />
                    </label>

                    <input onChange={() => { setView(false) }} type="radio" name="view" value="2" id="list" />
                    <label htmlFor="list">
                        <FaList />
                    </label>
                </div>
                <div className="main__view__options">
                    <span>Ordenar por</span>
                    <select defaultValue="default" name="price" id="price">
                        <option style={{ display: 'none' }} value="default">Preço</option>
                        <option value="high">Maior preço</option>
                        <option value="low">Menor preço</option>
                    </select>
                </div>
            </div>
            {products.items &&
                <div className="main__showcase">
                    <div className="main__showcase--products" style={view ? grid : list}>
                        {products.items.map((item, key) => (
                            <Card
                                key={item.id}
                                name={item.name}
                                image={item.image}
                                price={item.price}
                                specialPrice={item.specialPrice}
                                filter={item.filter[0]}
                            />
                        ))}
                    </div>
                </div>
            }
        </section>
    );
}

export default Shop;
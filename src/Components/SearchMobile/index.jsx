import React, { useEffect } from 'react';
import './searchMobile.scss';

const SearchMobile = ({ display, setDisplay }) => {

    useEffect(() => {
        const handleMenuMobile = () => {
            let el = document.querySelector('.header__search__mobile');
            display ? el.style.display = "flex" : el.style.display = "none";
        }

        handleMenuMobile();
    }, [display])

    return (
        <div className="header__search__mobile">
            <input type="text" />
            <button>Buscar</button>
        </div>
    );
}

export default SearchMobile;
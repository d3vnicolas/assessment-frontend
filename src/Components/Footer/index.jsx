import React from 'react';
import './footer.scss';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer__container">
                <div className="footer__content"></div>
            </div>
        </footer>
    );
}
 
export default Footer;
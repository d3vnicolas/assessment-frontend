import React, { useContext, useEffect, useState } from 'react';
import './filters.scss';
import { PageContext } from '../../Config/Context/pages';

const Filters = () => {

    const { products, setGender, setFilter } = useContext(PageContext);
    const [colors, setColors] = useState([]);

    useEffect(() => {
        const handleColorFilters = () => {
            if (products.filters) {
                if (products.filters[0].color) {
                    let handleColors = [];

                    products.items.map(item => {
                        if (!handleColors.includes(item.filter[0].color)) {
                            handleColors.push(item.filter[0].color);
                        }
                    })
                    setColors(handleColors);
                }
            }
        }


        handleColorFilters();
    }, [products, setColors]);

    return (
        <aside className="filters">
            {products.filters &&
                <div className="filters__content">
                    <h1>Filtre por</h1>
                    {Object.keys(products.filters[0])[0] === 'color' &&
                        <div className="filters__types filters__types--colors">
                            <h2 className="filters__subtitles">Cores</h2>
                            {colors.length > 0 &&
                                <ul>
                                    {colors.map((color, key) => (
                                        <li key={key}>
                                            <input type="radio" name="color" id={color} />
                                            <label onClick={() => setFilter({type: 'color', value: color})} htmlFor={color}>{color}</label>
                                        </li>
                                    ))}
                                </ul>
                            }
                        </div>
                    }
                    {Object.keys(products.filters[0])[0] === 'gender' &&
                        <div className="filters__types">
                            <h2 className="filters__subtitles">Gênero</h2>
                            <ul>
                                <li>
                                    <input type="radio" id="masc" name="gender"/>
                                    <label onClick={() => setFilter({type: 'gender', value: 'Masculina'})} htmlFor="masc">Masculino</label>
                                </li>
                                <li>
                                    <input type="radio" id="fem" name="gender"/>
                                    <label onClick={() => setFilter({type: 'gender', value: 'Feminina'})} htmlFor="fem">Feminina</label>
                                </li>
                            </ul>
                        </div>
                    }
                </div>
            }
        </aside >
    );
}

export default Filters;
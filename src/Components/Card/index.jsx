import React, { useContext, useEffect, useState } from 'react';
import './card.scss';
import { PageContext } from '../../Config/Context/pages';
import { GlobalContext } from '../../Config/Context/global';

const Card = (props) => {

    const { view, filter }  = useContext(PageContext);
    const { products } = useContext(GlobalContext)
    const [filtered, setFiltered] = useState(false);
    
    useEffect(() => {
        setFiltered(false);
        if(Object.keys(props.filter)[0] === 'gender'){
            if(filter.value !== undefined){
                if(props.filter.gender !== filter.value){
                    setFiltered(true);
                }
            }
        }else if(Object.keys(props.filter)[0] === 'color'){
            if(filter.value !== undefined){
                if(props.filter.color !== filter.value){
                    setFiltered(true);
                }
            }
        }
    }, [filter, props.filter])

    useEffect(() => {
        setFiltered(false);
    }, [products])

    return (
        <div className="card" style={filtered ? {display: 'none'} : {display: 'block'}}>
            <div className="card__content" style={view ? {flexDirection: 'column'} : {flexDirection: 'row'}}>
                <div className="card__image" style={view ? { background: `url(/${props.image}) center center no-repeat` } : { background: `url(/${props.image}) center center no-repeat`, width: '30%' }} >
                </div>
                <div className="card__details">
                    <p> {props.name} </p>
                    {props.specialPrice ?
                        <div className="card__special">
                            <span> {props.price.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </span>
                            <h2> {props.specialPrice.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </h2>
                        </div>
                        :
                        <h2> {props.price.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </h2>
                    }
                    {view === false &&
                        <a className="card__buy" href="/">Comprar</a>
                    }
                </div>
                {view &&
                    <a className="card__buy" href="/">Comprar</a>
                }
            </div>
        </div>
    );
}

export default Card;
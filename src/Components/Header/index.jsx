import React, { useContext, useState } from 'react';
import { GlobalContext } from '../../Config/Context/global';
import webjump from '../../Images/webjump.png';
import { GoThreeBars } from 'react-icons/go';
import { GrClose } from 'react-icons/gr';
import { BiSearch } from 'react-icons/bi';
import './header.scss';
import { Link } from 'react-router-dom';
import MenuMobile from '../MenuMobile';
import SearchMobile from '../SearchMobile';

const Header = () => {

    const { menu } = useContext(GlobalContext);
    const [menuMobile, setMenuMobile] = useState(false);
    const [searchMobile, setSearchMobile] = useState(false);

    return (
        <header>
            <div className="header__top">
                <div className="header__container">
                    <div className="header__access">
                        <p>
                            <a href="/">Acesse sua conta</a> ou <a href="/">cadastre-se</a>
                        </p>
                    </div>
                </div>
            </div>
            <div className="header__middle">
                <div className="header__container">
                    <div className="header__banner">
                        <div className="header__banner--logo">
                            <img src={webjump} alt="Logo Webjump!" title="Logomarca Webjump!" />
                        </div>
                        <div className="header__banner--search mobile-hidden">
                            <input type="text" />
                            <button>Buscar</button>
                        </div>
                    </div>
                    <MenuMobile display={menuMobile} setDisplay={setMenuMobile} />
                    <SearchMobile display={searchMobile} setDisplay={setMenuMobile} />
                </div>
                {menuMobile ?
                    <GrClose onClick={() => menuMobile ? setMenuMobile(false) : setMenuMobile(true)} className="header__menu__mobile--icon mobile-show desktop-hidden" />
                    :
                    <GoThreeBars onClick={() => menuMobile ? setMenuMobile(false) : setMenuMobile(true)} className="header__menu__mobile--icon mobile-show desktop-hidden" />
                }
                {searchMobile ?
                    <GrClose onClick={() => searchMobile ? setSearchMobile(false) : setSearchMobile(true)} className="header__search__mobile--icon" />
                    :
                    <BiSearch onClick={() => searchMobile ? setSearchMobile(false) : setSearchMobile(true)} className="header__search__mobile--icon mobile-show desktop-hidden"/>
                }
            </div>
            <div className="header__bottom mobile-hidden">
                <div className="header__container">
                    <nav className="header__menu">
                        <ul>
                            <li><Link to="/">Página inicial</Link></li>
                            {menu.map((item, key) => (
                                <li key={key}><Link to={`/${item.path}/${item.id}`}>{item.name}</Link></li>
                            ))}
                            <li><Link to="/contato">Contato</Link></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    );
}

export default Header;
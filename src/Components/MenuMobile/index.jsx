import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../../Config/Context/global';
import { Link } from 'react-router-dom';
import './menuMobile.scss';

const MenuMobile = ({ display, setDisplay }) => {

    const { menu } = useContext(GlobalContext);
    useEffect(() => {
        const handleMenuMobile = () => {
            let el = document.querySelector('.header__menu__mobile');
            display ? el.style.display = "block" : el.style.display = "none";
        }

        handleMenuMobile();
    }, [display])

    return (
        <nav className="header__menu__mobile">
            <ul>
                <li ><Link onClick={() => setDisplay(false)} to="/">Página inicial</Link></li>
                {menu.map((item, key) => (
                    <li key={key}><Link onClick={() => setDisplay(false)} to={'/' + item.path+'/'+item.id}>{item.name}</Link></li>
                ))}
                <li><Link onClick={() => setDisplay(false)} to="/contato">Contato</Link></li>
            </ul>
        </nav>
    );
}

export default MenuMobile;
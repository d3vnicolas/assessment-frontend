import React from 'react';
import Home from '../Pages/Home';
import Camisetas from '../Pages/Camisetas';
import Calcas from '../Pages/Calcas';
import Sapatos from '../Pages/Sapatos';
import { Routes as Switch, Route } from 'react-router-dom';
import PageProvider from '../Config/Context/pages';

const Routes = () => {
    return (
        <PageProvider>
            <Switch>
                <Route path="/" element={<Home />} />
                <Route path="/camisetas/:id" element={<Camisetas />} />
                <Route path="/calcas/:id" element={<Calcas />} />
                <Route path="/sapatos/:id" element={<Sapatos />} />
                <Route path="*" element={<h2 align="center" style={{marginTop: '128px', fontSize: '1.8rem'}}>Página não encontrada. (404)</h2>} />
            </Switch>
        </PageProvider>
    );
}

export default Routes;
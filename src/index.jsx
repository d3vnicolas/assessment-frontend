import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/global.scss';
import GlobalProvider from './Config/Context/global';

ReactDOM.render(
  <React.StrictMode>
    <GlobalProvider>
      <App />
    </GlobalProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

import React, { createContext, useState} from 'react';

export const GlobalContext = createContext({});

export default function GlobalProvider({ children }) {
    const [menu, setMenu] = useState([]);
    const [products, setProducts] = useState([]);
    const [view, setView] = useState(true);

    return(
        <GlobalContext.Provider
            value={{
                menu, setMenu,
                products, setProducts,
                view, setView,
            }}
        >
            {children}
        </GlobalContext.Provider>
    );
}

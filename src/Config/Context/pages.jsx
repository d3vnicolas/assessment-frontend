import React, { createContext, useState } from 'react';

export const PageContext = createContext({});

export default function PageProvider({ children }){

    const [products, setProducts] = useState([]);
    const [view, setView] = useState(true);
    const [filter, setFilter] = useState({});

    return(
        <PageContext.Provider
            value={{
                products, setProducts,
                view, setView,
                filter, setFilter
            }}
        >
            {children}
        </PageContext.Provider>
    );
}

const API_BASE = "https://61e0724463f8fc00176187b3.mockapi.io/api/V1/categories/";


const basicFetch = async (endpoint) => {
    const request = await fetch(`${API_BASE}${endpoint}`);
    const response = await request.json();
    return response;
}

const getData = {
    header: async () => {
        return await basicFetch("list");
    },
    products: async (id) => {
        return await basicFetch(id);
    }
}

export default getData;

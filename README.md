## Olá avaliadores, aqui vou relatar os processos e decisões que adotei na minha solução.

<hr>

## Informações 📖

- Primeiramente resolvi utilizar a biblioteca React com jsx portanto, tomei a liberdade de alterar o ambiente de desenvolvimento, para melhorar a produtividade. Espero que não atrapalhe na análise do desafio.

- Com a **API**, retirei ela do ambiente e vou mockar no <a href="https://mockapi.io/"> mockapi.io </a> já que, ela estava sendo executada de forma que atrapalhava o uso das dependências React.
Agora está acessível através do link: <kbd>https://61e0724463f8fc00176187b3.mockapi.io/api/V1/categories/:endpoint</kbd>

- Para a estilização dos componentes entre styled-components e Sass, optei pela segunda opção no formato scss aplicando os conceitos BEM, já que, o desafio tem um layout simples e pouco complexo.

- Na conexão com a API decidi usar métodos nativos com funções assíncronas.
Adotei o uso de **hooks**, useEffect para receber os dados e useState para exibição. 

- Para fazer a navegação do usuário optei por utilizar o plugin **react-router-dom.**

- Com o objetivo de evitar o "props drilling" em alguns componentes, implementei um contexto global para os menus e, outro contexto para as páginas de cada produto.

- Para o funcionamento dos modos de visualização, utilizei javascript injetando estilos inline.

- O desenvolvimento dos filtros foi limitado pois, a api é bem simples e não oferece tantos recursos. Ainda assim, coloquei ao menos um filtro para cada página de produtos.

- Para filtrar os produtos determinei que, ao ser aplicado qualquer filtro, um estado se atualiza, e é passado para o componente **card** onde, se valida o tipo de filtro e, o próprio componente que envolve o produto recebe um estilo css inline via javascript.


## Tecnologias utilizadas 🚀

- ReactJS
- Hooks
- Scss
- Routes
- ContextAPI
- React icons

## Observações 🔍

No decorrer do desenvolvimento notei que a api fornecida continha algumas incoerências. Que pode ser firmado na seção de filtros de cores da página "sapatos", onde as imagens não se condiz com o filtro selecionado. Além disso, O desenvolvimento dos filtros foi limitado pois, a api é bem simples e não oferece tantos recursos. Ainda assim, coloquei ao menos um filtro para cada página de produtos.


## Conclusão 

Infelizmente alguns detalhes não foram possíveis terminar, como: ordem por preço e campo de busca.
A todo momento me esforcei ao máximo para entregar o melhor resultado, tive menos tempo do que gostaria para investir no desafio mas, acredito que meu desempenho tenha sido bom.

Espero que a forma como alterei o ambiente não atrapalhe a avaliação mas, além de todos os arquivos e explicações, vou disponibilizar um link para acessar a minha solução. Quero ressaltar que este link demonstra minha solução exatamente igual a que será enviada pelo git.

link da solução online na vercel: <a href="https://webjump-desafio-liart.vercel.app/">desafio webjump</a>

Obrigado pela oportunidade.

contato: <br>
E-mail: <a href="mailto:devnicolas.coroa@gmail.com">devnicolas.coroa@gmail.com</a><br>
Telefone: (11)9 9878 - 5793<br>
LinkedIn: <a href="https://www.linkedin.com/in/nicolas-soares-887655220/">Nicolas Soares</a><br>
GitHub: <a href="https://github.com/d3vnicolas">d3vnicolas</a>